import java.awt.event.*;
import java.awt.*;
public class Generator extends Thread implements PulseSource
{
    
    private ActionListener a1;
    private boolean alive = true;
    private boolean on = false;
    public boolean first = true;
    
    private int pulseDelay;
    private int pulseCount;
    public int counter = 0;
    public int control = 0;
    public int controlfit = 0;
    
    
    final static byte BURST_MODE = 0;
    final static byte CONTINOUS_MODE = 1;
    public static byte MODE = CONTINOUS_MODE;

    public Generator(){
        start();
    }
    public void run(){
        while(alive){
            try{
                if(on){
                    if(MODE == CONTINOUS_MODE){
                        counter++;
                        event();
                        Thread.sleep(pulseDelay);
                    }else{
                        burst();
                        halt();
                    }
                    Thread.sleep(1);
                } else {
                    Thread.sleep(50);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();   
            }
        }
    }
    public void burst(){
        try{
                controlfit = 0;
                for(control = 0; control < pulseCount; control++){
                    counter++;
                    controlfit++;
                    event();
                    Thread.sleep(pulseDelay);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();   
            }
    }
    public void event(){
        if(a1!=null){
            a1.actionPerformed(new ActionEvent(this, 
            ActionEvent.ACTION_PERFORMED, 
            "Tick"));
        }   
    }
    public void addActionListener(ActionListener p1){
        a1=AWTEventMulticaster.add(a1, p1);
    }
    public void removeActionListener(ActionListener p1){
        a1=AWTEventMulticaster.remove(a1, p1);
    }
    public void trigger() {
        alive = true;
        on = true;
    }
    public void setMode(byte mode) {
        if (mode == 1){
            MODE = 1;
        } else {
            MODE = 0;
        }
    }
    public byte getMode() {
        return MODE;
    }
    public String getModeStr(){
        if(MODE == 0){
            return "ON";
        } else {
            return "OFF";
        }
    }
    public void halt() {
        on = false;
    }
    public void setPulseDelay(int ms){
        pulseDelay = ms;
    }
    public int getPulseDelay(){
        return pulseDelay;  
    }
    public void setPulseCount(int burst){
        pulseCount = burst;
    }
    public int getPulseCount(){
        return pulseCount;
    }
}


 

