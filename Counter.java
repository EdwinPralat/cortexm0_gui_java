
/**
 * @author (Edwin_Pralat)
 * @version (1.1)
 */


public class Counter implements Cortex_M0_SysTic_Interface
{
    private int CVR = 0;
    private int RVR = 0;
    private int CSR = 0;  
    private boolean init = true;
    private boolean tickint = false;
    private boolean enableflag = false;
    private boolean interruptflag = false;
    private boolean countflag = false;
    
    public void clearfg(){
        CSR &= ~(1 << 0x10);
    }
    public boolean Source(){
        if(((CSR >> 0x2) & 0x1) == 1){
            clearfg();
            return true;
        } else {
            clearfg();
            return false;
        }
    }
    public void tickInternal(){
         if(isEnableFlag()){
             if(Source()){
                 ticks();
             }
         }                    
    }
    public void tickExternal(){
         if(isEnableFlag()){
             if(!Source()){
                 ticks();
             }
         }                         
    }
    public void ticks(){        
        if(init){
            setCVR(RVR);
            tickint = false;
            init = false;
                            
            if(RVR == 0){
                setDisable();
            } 
        } else {
            if(CVR == 1){
                CVR--;
                CSR |= (1 << 0x10);
                if(isInterruptFlag()){
                    tickint = true;   
                }
            } else if(CVR == 0){
                setCVR(RVR);
                tickint = false;
                if(RVR == 0){
                    setDisable();
                } 
            } else {
                CVR--;
                clearfg();
            }
        }
    }
    public void setCVR(int CVRvalue){
        CVR = (CVRvalue & 0xFFFFFF);
        clearfg();
    }
    public void setRVR(int RVRvalue){
        RVR = (RVRvalue & 0xFFFFFF);
    }
    public void setCSR(int CSRvalue){
        CSR = CSRvalue;
    }
    public void reset(){
        CSR = 0x00000000;
    }
    public void setEnable(){
        CSR |= 0x00000001;        
        clearfg();
        init = true;
    }
    public void setDisable(){
        CSR &= ~(1 << 0x00000000);
        clearfg();
    }
    public void setSource(boolean source){
        if(source ){
            CSR |= (1 << 0x2);
            clearfg();
        } else{
            CSR &= ~(1 << 0x2);
            clearfg();
        }
    }
    public void setInterruptEnable(){
        CSR |= (1 << 0x1);
        clearfg();
    }
    public void setInterruptDisable(){
        CSR &= ~(1 << 0x1);
        clearfg();
    }
    public int getCVR(){
        return CVR;
    }
    public int getRVR(){
        return RVR;
    }
    public int getCSR(){
        int tempCSR = CSR;
        clearfg();
        return tempCSR;
    }
    public boolean getEnabled(){
        if ((CSR & 0x1) == 1){
            clearfg();
            return true; 
        } else {
            clearfg();
            return false;
        }
    }
    public boolean getInterrupt(){
        if (((CSR >> 0x1) & 0x1) == 1){
            clearfg();
            return true;
        } else {
            clearfg();
            return false;
        }
    }
    public boolean isCountFlag(){
        if (((CSR >> 0x10) & 0x1) == 1){
            countflag = true;
        } else {
            countflag = false;
        }
        return countflag;
    }
    public boolean isEnableFlag(){
        if((CSR & 0x1) == 1){
            enableflag = true;   
        } else {
            enableflag = false;   
        }
        return enableflag;
    }
    public boolean isInterruptFlag(){
        if(((CSR >> 0x1) & 0x1) == 1){
            interruptflag = true;
        } else {
            interruptflag = false;
        }
        return interruptflag;
    }
    public String interruptStr(){
        if(isInterruptFlag()){
            return "Enabled";
        } else {
            return "Disabled";
        }
    }
    public boolean isInterrupt(){
        return tickint;
    }
    public String interruptOccured(){
        if (tickint == true){
            return " occured";
        } else {
            return "n't occured";
        }
    }
    public String toString(){
        return ("SysTick Counter\n CVR = " + CVR + " RVR = " + RVR +"\n"); 
    }
    public String sourceStr(){
        if(Source()){
            return "Internal";
        } else{
            return "External";
        }
    }
    public String enableStr(){
        if(isEnableFlag()){
            return "Enabled";
        } else{
            return "Disabled";
        }
    }
}
