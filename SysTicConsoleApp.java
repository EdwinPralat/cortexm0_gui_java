import java.util.Scanner;

public class SysTicConsoleApp
{
    Counter myCounter;
    Scanner input = new Scanner(System.in);
    private int opcja;
    private int newRVR;
    private int wielok;
    public SysTicConsoleApp(){
         menu();
    }
    private int menu(){
     myCounter = new Counter();
     display("1 - przykladowe uzycie programu\n2 - uzycie programu z danymi uzytkownika");
     display("0 - konczy prace programu");
        
     opcja = input.nextInt();
     
     if(opcja==0){
         return 0;
     } else if (opcja == 1){
         makePokaz();
     } else if (opcja == 2){
         pokazUzytkownika();
     } else {
        display("Zly wybor");
        menu();
     }
     return 0;
    }
    private void display(String tekst){
        System.out.println(tekst);
    }
    private void makePokaz(){
        display("Stan poczatkowy: " + myCounter.toString());
        myCounter.setRVR(8);
        display("Ustawiam rejestr RVR na wartosc " + myCounter.getRVR());
        myCounter.setSource(true);
        display("A zrodlo na wewnetrzne\n");
        
        display("Wysylam 4 impulsy");
        for (int i=0; i<5; i++){
            myCounter.tickInternal();
        }
        display("Teraz: " + myCounter);
        display("Dlaczego nie zadzialalo?\n");
        display("Flaga Enable ma wartosc " + myCounter.isEnableFlag());
        myCounter.setEnable();
        display("Wiec aby licznik mogl dzialac, odpowiedni bit zmieniamy na " + myCounter.isEnableFlag());  
        for (int i=0; i<5; i++){
            myCounter.tickInternal();
        }
        display("Teraz: " + myCounter);
        for (int i=0; i<4; i++){
            myCounter.tickInternal();
        }
        display("Doliczamy do 0 \n" + myCounter);
        myCounter.tickInternal();
        display("Po ''przekroczeniu'' wartosci 0 nastepuje reload:\n " + myCounter);
        
     }
    private void pokazUzytkownika(){
        display("Stan poczatkowy: " + myCounter.toString());
        display("Jaka wartosc poczatkowa wybierasz?");
            
        newRVR = input.nextInt();
        myCounter.setRVR(newRVR);
        
        display("Teraz " + myCounter);
        display("Dodatkowo ustawiam zrodlo na Internal oraz wlaczam bit Enable.");
        myCounter.setSource(true);
        myCounter.setEnable();
        display("Ile zliczen chcesz wykonac?");
        wielok = input.nextInt();
        for (int i=0; i<wielok; i++){
            myCounter.tickInternal();
        }
        display("" + myCounter);
        display("Dziekuje za wspolna zabawe");
        }
    public static void main(String[] arg){
         new SysTicConsoleApp();
    }
}
