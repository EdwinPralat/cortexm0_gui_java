
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GUI extends JFrame implements ActionListener{
    
	private static final long serialVersionUID = 1L;
	
	static final int MIN = 0;
    static final int rvrMAX = 20;
    static final int rvrINIT = 0;
    private int fwidth = 700;
    private int fheight = 760;
    private int CSR;
    private int triggered = 0;
    
    
    private Knob knob = new Knob();
    private Counter cortex = new Counter();
    private Generator generator = new Generator();
    private JSlider slider = new JSlider(JSlider.VERTICAL,MIN, rvrMAX, rvrINIT);
    private JFrame frame = new JFrame("CortexM01 SysTick GUI");
    private Rectangle d = frame.getBounds();
    
    private JButton start = new JButton("Start");
    private JButton stop = new JButton("Stop");
    private JButton reset = new JButton("Reset All");
    private JButton resetCVR = new JButton("Set CVR");
    private JButton externalTick = new JButton("External Source Trigger");
    private JRadioButton enable = new JRadioButton(" Counter: On = Enabled, Off = Disabled");
    private JRadioButton internal = new JRadioButton(" Source: On = Internal, Off = External" );
    private JRadioButton burst = new JRadioButton(" Burst mode: On/Off" );
    private JRadioButton interruptSet = new JRadioButton("Interrupt: On = Enabled, Off = Disabled");    
    private JSpinner delaySet = new JSpinner(); 
    
    private JTextField showRVR = new JTextField(" RVR Value = " + cortex.getRVR());
    private JTextField showCVR = new JTextField(" CVR Value = " + cortex.getCVR());
    private JTextField showEnableFlag = new JTextField(" Counter Status: " + cortex.enableStr());
    private JTextField showCountFlag = new JTextField(" Count Flag = " + cortex.isCountFlag());
    private JTextField showDelay = new JTextField(" Delay = " + generator.getPulseDelay() + "ms");
    private JTextField showBurst = new JTextField(" Executions quantity = " + generator.getPulseCount());
    private JTextField showCSRint = new JTextField(" CSR(int) = " + cortex.getCSR());
    private JTextField showCSRhex = new JTextField(" CSR(bin) = " + Integer.toBinaryString(cortex.getCSR()));
    private JTextField showSource = new JTextField(" Source: " + cortex.sourceStr());
    private JTextField showBurstMode = new JTextField(" Burst mode: " + generator.getModeStr());
    private JTextField showInterrupt = new JTextField(" Interrupt: " + cortex.interruptStr());
    private JTextField showInterruptHappened = new JTextField(" Interrupt have");
    
    private JLabel counted = new JLabel(" Cycles passed: " + generator.counter);
    private JLabel control = new JLabel(" Burst cycles passed: " + (generator.controlfit));
    private JLabel externalTicks = new JLabel("External source cycles triggered: " + triggered);
    
    private JPanel north = new JPanel(new GridLayout(2,2));
    private Container z = getContentPane();
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == stop){
            generator.halt();
        }
        if(e.getSource() == reset){
            cortex.setCVR(0);
            slider.setValue(rvrINIT);
            generator.halt();
            generator.setMode(Generator.CONTINOUS_MODE);
            knob.reset();
            generator.setPulseCount(knob.getBurstVal());
            delaySet.setValue(500);
            cortex.reset();
            internal.setSelected(false);
            burst.setSelected(false);
            enable.setSelected(false);
            interruptSet.setSelected(false);
            generator.first = true;
            generator.control = 0;
            generator.counter = 0;
            generator.controlfit = 0;
            triggered = 0;
        }
        if(e.getSource() == resetCVR){
            cortex.setCVR(cortex.getRVR());
        }
        if(e.getActionCommand() == "knob"){
            generator.setPulseCount(knob.getBurstVal());
        }
        if(e.getSource() == enable){
            if(enable.isSelected() == true){
                cortex.setEnable();
            } else if(enable.isSelected() == false){
                cortex.setDisable();
            }
        }
        if(e.getSource() == burst){
            if(burst.isSelected()){
                generator.setMode(Generator.BURST_MODE);
            } else {
                generator.setMode(Generator.CONTINOUS_MODE);
            }
        }
        if(e.getSource() == internal){
            if(internal.isSelected()){
                cortex.setSource(true);
            } else {
                cortex.setSource(false);
            }
        }
        if(e.getActionCommand()== "Tick"){
            if(cortex.Source()){
                cortex.tickInternal();
            }
        }
        if(e.getSource()== interruptSet){
            if(interruptSet.isSelected()){
                cortex.setInterruptEnable();
            } else {
                cortex.setInterruptDisable();
            }
        }
        if(e.getSource()== externalTick){
            if(!cortex.Source()){
                cortex.tickExternal();
                triggered++;
                generator.counter++;
            }
        }
        update();
    }
    public void update(){
        showRVR.setText(" RVR Value = " + cortex.getRVR());
        showCVR.setText(" CVR Value = " + cortex.getCVR());
        showEnableFlag.setText(" Counter Status: " + cortex.enableStr());
        showCountFlag.setText(" Count Flag = " + cortex.isCountFlag());
        showDelay.setText(" Delay = " + generator.getPulseDelay() + "ms");
        showBurst.setText(" Executions quantity = " + generator.getPulseCount());
        CSR = cortex.getCSR();
        showCSRint.setText(" CSR(int) = " + CSR);
        showCSRhex.setText(" CSR(bin) = " + Integer.toBinaryString(CSR));
        showBurstMode.setText(" Burst mode: " + generator.getModeStr());
        showSource.setText(" Source: " + cortex.sourceStr());
        showInterrupt.setText(" Interrupt: " + cortex.interruptStr());
        showInterruptHappened.setText(" Interrupt have" + cortex.interruptOccured());
        counted.setText(" Cycles passed: " + generator.counter);
        control.setText(" Burst cycles passed: " + (generator.controlfit));
        externalTicks.setText("External source cycles triggered: " + triggered);
    }
    public GUI(){
        frame.setSize(new Dimension(fwidth,fheight));
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        generator.addActionListener(this);
        
        start.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                    if(cortex.Source()){
                    generator.trigger();
                } else{
                    generator.halt();
                }
                update();
            }
        });
        z.add(start);
        
        stop.addActionListener(this);
        z.add(stop);
        
        reset.addActionListener(this);
        z.add(reset);
        
        resetCVR.addActionListener(this);
        z.add(resetCVR);
        
        externalTick.addActionListener(this);
        z.add(externalTick);
        z.add(externalTicks);
        
        enable.addActionListener(this);
        enable.setSelected(false);
        north.add(enable);
        
        internal.addActionListener(this);
        internal.setSelected(false);
        north.add(internal);
        
        burst.addActionListener(this);
        burst.setSelected(false);
        north.add(burst);
        
        interruptSet.addActionListener(this);
        interruptSet.setSelected(false);
        north.add(interruptSet);
        
        z.add(north,BorderLayout.NORTH);
        
        delaySet.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e) { 
                generator.setPulseDelay((int)delaySet.getValue());
                update();
            }
        });
        z.add(delaySet);
        
        delaySet.setValue(500);
        JLabel delayLabel = new JLabel("Set Delay value");
        z.add(delayLabel);

        slider.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e) { 
                JSlider slider = (JSlider)e.getSource();
                if (!slider.getValueIsAdjusting()) { 
                     cortex.setRVR((int)slider.getValue());
                     update();
                } 
            }
        });
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setPaintTrack(true);
        slider.setMajorTickSpacing(2);
        slider.setMinorTickSpacing(1);
        slider.setBorder(BorderFactory.createTitledBorder("RVR"));
        z.add(slider,BorderLayout.WEST);
        
        showRVR.setEnabled(true);
        showRVR.setEditable(false);
        z.add(showRVR);
        
        showCVR.setEnabled(true);
        showCVR.setEditable(false);
        z.add(showCVR);
        
        showDelay.setEnabled(true);
        showDelay.setEditable(false);
        z.add(showDelay);
        
        showBurst.setEnabled(true);
        showBurst.setEditable(false);
        z.add(showBurst);
        
        showCSRint.setEnabled(true);
        showCSRint.setEditable(false);
        z.add(showCSRint);
        
        showCSRhex.setEnabled(true);
        showCSRhex.setEditable(false);
        z.add(showCSRhex);
        
        showCountFlag.setEnabled(true);
        showCountFlag.setEditable(false);
        z.add(showCountFlag);
        
        showEnableFlag.setEnabled(true);
        showEnableFlag.setEditable(false);
        z.add(showEnableFlag);
        
        showSource.setEnabled(true);
        showSource.setEditable(false);
        z.add(showSource);
        
        showBurstMode.setEnabled(true);
        showBurstMode.setEditable(false);
        z.add(showBurstMode);
        
        showInterrupt.setEnabled(true);
        showInterrupt.setEditable(false);
        z.add(showInterrupt);
        
        showInterruptHappened.setEnabled(true);
        showInterruptHappened.setEditable(false);
        z.add(showInterruptHappened);
        
        z.add(control);
        
        z.add(counted);
        
        knob.addActionListener(this);
        z.add(knob);
        
        
        frame.addComponentListener(new ComponentAdapter(){
            public void componentResized(ComponentEvent e){
                d = frame.getBounds();
                int wid = w(300);
                int hei = h(50);
                
                start.setSize(w(150),h(60));
                start.setLocation(60,55);
                stop.setBounds(65 + w(150),55,w(150),h(60));
                reset.setBounds(70 + (2*w(150)),55,w(150),h(60));
                resetCVR.setBounds(75 + (3*w(150)),55,w(150),h(60));
                delaySet.setBounds(60,70 + h(60),wid,hei-h(20));
                delayLabel.setBounds(20 + w(150),75+h(85),wid,h(20));
                
                showEnableFlag.setBounds(60,90 + h(100),wid,hei);
                showSource.setBounds(60,90 + h(160),wid,hei);
                showBurstMode.setBounds(60,90 + h(220),wid,hei);
                showDelay.setBounds(60,90 + h(280),wid,hei);
                showRVR.setBounds(60,90 + h(340),wid,hei);
                showBurst.setBounds(70 + wid,90 + h(340),wid,hei);
                showInterrupt.setBounds(60,90 + h(400),wid,hei);
                showInterruptHappened.setBounds(70 + wid,90 + h(400),wid,hei);
                showCVR.setBounds(60,90 + h(460),wid,hei);   
                showCSRint.setBounds(70 + wid,90 + h(460),wid,hei);
                showCountFlag.setBounds(60,90 + h(520),wid,hei);
                showCSRhex.setBounds(70 + wid,90 + h(520),wid,hei);
                
                counted.setBounds(20 + w(210),h(620)+50,wid,h(20));
                control.setBounds(10 + w(50),h(620)+50,wid,h(20));
                externalTick.setBounds(70 + wid,90 + h(580),wid,hei);
                externalTicks.setBounds(15 + w(50),h(640)+50,wid,h(20));
            }
        });
        
        frame.setMinimumSize(new Dimension(fwidth,fheight));
        frame.setContentPane(z);
        frame.setVisible(true);
    }
    public int w(int resWidth){
        return ((d.width-80)*resWidth/(fwidth-80)); 
    }
    public int h(int resHeight){
        return (d.height*resHeight/fheight);
    }
   public static void main(String[] args){
        new GUI();   
    }
}
