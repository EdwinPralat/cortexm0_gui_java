import javax.swing.JComponent;
import java.awt.*;
import java.awt.event.*;

public class Knob extends JComponent implements MouseListener, MouseMotionListener
{
	private static final long serialVersionUID = 1L;
	private int x;
    private int y;
    public int diameter;
    private int ovaldiameter;
    private int midX;
    private int midY;
    private double sinus;
    private double cosinus;
    private double wodz;
    private int knobX;
    private int knobY;
    private int burst_val;
    private ActionListener a1;
    
    private Color writing;
    private Color digits;
    private Color center;
    private Color knob;
    private Color oval;
    private Color background;
    private boolean click = false;
    private boolean first = true;
    
    public Knob(){
        this(0);
    }
    public Knob(int burst_val){
        knob = new Color(0x00, 0x00, 0x25);
        oval = new Color(0x00, 0xBB, 0x00);
        background = new Color(0xA0,0xA0,0xA0);
        center = new Color(0x00,0x00,0x00);
        digits = new Color(0xFF,0xFF,0xFF);
        writing = new Color(0x00,0x00,0x00);
        addMouseListener(this);
        addMouseMotionListener(this);
    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Dimension d = getSize();
        diameter = Math.min(d.width,d.height)*11/27;
        ovaldiameter = diameter/12;
        midX = d.width*93/128 + 10;
        midY = d.height*37/128 + 20;
       
        if(first){
             knobY = -(3*diameter/8);
             first = false;
        }
       
        g.setColor(background);
        g.fillRect(0,0,d.width,d.height);
        g.setColor(knob);
        g.fillOval(midX-diameter/2,midY-diameter/2,diameter,diameter);
        g.setColor(center);
        g.fillOval(midX-diameter/200,midY-diameter/200,diameter/100,diameter/100);
        g.setColor(oval);
        g.fillOval(midX - knobX - ovaldiameter/2,midY + knobY - ovaldiameter/2,ovaldiameter,ovaldiameter);
        
        g.setColor(digits);
        g.drawString("0",midX-2,midY-diameter/2+20);
        g.drawString("3",(int)(midX+0.707*diameter/2-11),(int)(midY-0.707*diameter/2+15));
        g.drawString("6",midX+diameter/2-20,midY+5);
        g.drawString("9",(int)(midX+0.707*diameter/2-11),(int)(midY+0.707*diameter/2-7));
        g.drawString("12",midX-6,midY+diameter/2-20);
        g.drawString("15",(int)(midX-0.707*diameter/2+7),(int)(midY+0.707*diameter/2-9));
        g.drawString("18",midX-diameter/2+20,midY+3);
        g.drawString("21",(int)(midX-0.707*diameter/2+11),(int)(midY-0.707*diameter/2+15));
        
        g.setColor(writing);
        g.drawString("Burst mode: amount of Cycles = " + burst_val, midX-90,midY+diameter/2+12);
    }
    @Override
     public void mouseClicked(MouseEvent e) {         
        position(e);
    }
    public void position(MouseEvent e){
        Point p = e.getPoint();
        if(p.distance(midX,midY)<diameter/2){
            x = -(p.x - midX);
            y = p.y - midY;
            wodz = Math.sqrt((double)(x*x+y*y));
            sinus = (y/wodz);
            cosinus = (x/wodz);
            knobX =(int)(cosinus * (3*diameter/8));
            knobY = (int)(sinus * (3*diameter/8));
            if(knobX == 0 && knobY == 0){
                 knobY = -(3*diameter/8);
            }
            burst_val = (int)(179 +Math.toDegrees(Math.atan2(x,y)))/15;
            repaint();
            if(a1!=null){
                a1.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "knob"));
            }   
        }
    }
    public int getBurstVal(){
        return burst_val;
    }
    @Override
    public void mouseEntered(MouseEvent e){
        background = new Color(0xA0,0xA5,0xA0);
        repaint();
    }
    @Override
    public void mouseExited(MouseEvent e) {
        background = new Color(0xA0,0xA0,0xA0);
        repaint();
    }
    @Override
    public void mousePressed(MouseEvent e) {
        click = true;
        position(e);
    }
    @Override
    public void mouseReleased(MouseEvent e) {
        click = false;
    }
    @Override
    public void mouseDragged(MouseEvent e) {
        if(click){
            position(e);
        }
    }
    @Override
    public void mouseMoved(MouseEvent e) {
        Point p = e.getPoint();
        if(p.distance(midX,midY)<diameter/2){
            knob = new Color(0x00, 0x00, 0x40);
            oval = new Color(0x00, 0xD0, 0x00);
            repaint();
        } else {
            knob = new Color(0x00, 0x00, 0x25);
            oval = new Color(0x00, 0xBB, 0x00);
            repaint();
        }
    }
    public void addActionListener(ActionListener p1){
        a1=AWTEventMulticaster.add(a1, p1);
    }
    public void removeActionListener(ActionListener p1){
        a1=AWTEventMulticaster.remove(a1, p1);
    }
    public void reset(){
        knobX = 0;
        knobY = -(3*diameter/8);
        burst_val = 0;
        repaint();
    }
}